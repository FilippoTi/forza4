/*
    Autore: Tiberio Filippo.
    Programma: Forza 4!
    Descrizione: Semplice programma che permette all'utente di giocare a forza 4.
    Data: 22/03/18
*/

// INCLUDO LE LIBRERIE
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <Windows.h>

// DICHIARO LE COSTANTI
#define SpazioVuoto ' '
#define SpazioUtente 'X'
#define SpazioAvversario 'O'
#define RigheVerticali 179
#define RigheOrizzontali 205
#define Testa 0
#define Croce 1
#define Errore -2
// DICHIARO LE VARIABILI
char NomeUtente[300];
char TabellaDiGioco[6][7];
int vittoria = 0;
int IndiceColonna[7]= {5,5,5,5,5,5,5};
int ColonnaSceltaDalGiocatore;
char ToC[500];
int TestaOCroceSceltoDalGiocatore;
int RisultatoTestaOCroce;
int ColonnaSceltaDalComputer=0;
int IndiceGenericoA, IndiceGenericoB;

// PROTOTITPI DI FUNZIONE
void Introduzione();
void Inizializzazione();
void StampoCampoDiGioco();
void TurnoGiocatore();
void TurnoComputer();
int ControlloVincita();
void SetColor(short color);

int main()
{
    vittoria=0;
    srand((unsigned)time(NULL));
    Introduzione();


    while(vittoria==0)
    {

        system("cls");

        system("cls");
        printf("Giocatore: %s [%c]\n\n\n",NomeUtente,SpazioUtente);
        printf("E' uscito: ");
        if(RisultatoTestaOCroce==0)
        {
            printf("Testa\n");
        }
        else if(RisultatoTestaOCroce==1)
        {
            printf("Croce\n");
        }
        else
        {
            printf("Errore!");
            return Errore;
        }

        if(RisultatoTestaOCroce==TestaOCroceSceltoDalGiocatore)
        {
            ControlloVincita();
            StampoCampoDiGioco();
            TurnoGiocatore();
            ControlloVincita();
            TurnoComputer();
            ControlloVincita();
        }
        else
        {
            ControlloVincita();
            TurnoComputer();
            ControlloVincita();
            StampoCampoDiGioco();
            TurnoGiocatore();
            ControlloVincita();
        }

    }
    system("cls");
    printf("\n\n");

    StampoCampoDiGioco();

    if (vittoria == 1)
    {
        printf("Complimenti %s hai vinto!!!\n",NomeUtente);
    }
    else if(vittoria == 2)
    {
        printf("    Mi dispiace ma questa volta il computer ti ha fregato!\n\n");
    }
    else
    {
        printf("Error!");
        return Errore;
    }
    _sleep(2500);
    system("cls");
    SetColor(14);
    printf("\n\n\n           Grazie di aver giocato con noi %s!\n",NomeUtente);
    return 0;
}

void Introduzione() // Stampo l'introduzione e registro il nome utente
{
    printf("\n\n        ##FORZA QUATTRO##");
    printf("\n       by Tiberio Filippo");
    printf("\n     Inserisci il tuo nome!\n ");
    fgets(NomeUtente,sizeof(NomeUtente),stdin);
    printf("\nRegistrato!");
    system("cls");
    Inizializzazione();
}

void Inizializzazione()// inizializzo le variabili a default
{
    srand((unsigned)time(NULL));
    int riga,colonna,i;
    for(riga=0; riga<7; riga++)
    {
        for(colonna=0; colonna<6; colonna++)
        {
            TabellaDiGioco[riga][colonna] = SpazioVuoto;
        }
    }

    for(i=0; i<sizeof(NomeUtente); i++)
    {
        if(NomeUtente[i]=='\n')
        {
            NomeUtente[i]=0;
        }
    }

    do
    {
        printf("\n\n\n         Testa o Croce\n      Inserimento: ");
        fgets(ToC,sizeof(ToC),stdin);
        for(i=0; i<sizeof(ToC); i++)
        {
            if(ToC[i]=='\n')
            {
                ToC[i]=0;
            }
        }
    }
    while(stricmp(ToC,"testa")!=0&&stricmp(ToC,"croce")!=0);
    if (stricmp(ToC,"testa")==0)
    {
        TestaOCroceSceltoDalGiocatore=0;
    }
    else if(stricmp(ToC,"croce")==0)
    {
        TestaOCroceSceltoDalGiocatore=1;
    }
    RisultatoTestaOCroce = rand()%2;
}

void StampoCampoDiGioco()   //stampa il campo da gioco
{
    int riga,colonna,i,j;
    printf("\n");
    for(riga=0; riga<6; riga++)
    {
        printf("                       ");
        for(colonna=0; colonna<7; colonna++)
        {
            printf("%c%c%c%c",RigheVerticali, SpazioVuoto,SpazioVuoto,SpazioVuoto);
        }
        printf("%c\n",RigheVerticali);
        printf("                       ");
        for(colonna=0; colonna<7; colonna++)
        {
            printf("%c%c",RigheVerticali,SpazioVuoto);
            if(TabellaDiGioco[riga][colonna]==SpazioUtente)
            {
                SetColor(2);
            }
            else if(TabellaDiGioco[riga][colonna]==SpazioAvversario)
            {
                SetColor(6);
            }
            printf("%c%c",TabellaDiGioco[riga][colonna],SpazioVuoto);
            SetColor(7);
        }
        printf("%c\n",RigheVerticali);
    }
    printf("                       ");
    for(i=0; i<7; i++)
    {
        printf("%c",RigheOrizzontali);
        printf("%c",RigheOrizzontali);
        printf("%c",RigheOrizzontali);
        printf("%c",RigheOrizzontali);
    }
    printf("%c",RigheOrizzontali);
    printf("\n                       ");
    for(j=0; j<7; j++)
    {
        printf("%c%c%i%c",RigheVerticali,SpazioVuoto,j+1,SpazioVuoto);
    }
    printf("%c\n\n",RigheVerticali);
}

void TurnoGiocatore() //giocata del giocatore
{
    printf("Turno Del Giocatore: %s\n",NomeUtente);
    do
    {
        printf("Scegli la colonna in cui inserire il gettone\nInserimeto: ");
        scanf("%d",&ColonnaSceltaDalGiocatore);
        if(ColonnaSceltaDalGiocatore<1||ColonnaSceltaDalGiocatore>7)
        {
            printf("Un numero da 1 a 7!!\n");
        }
        if(IndiceColonna[ColonnaSceltaDalGiocatore-1]<0)
        {
            printf("Limite colonna raggiunto!!\n");
        }
    }
    while(ColonnaSceltaDalGiocatore<1||ColonnaSceltaDalGiocatore>7||IndiceColonna[ColonnaSceltaDalGiocatore-1]<0);

    TabellaDiGioco[IndiceColonna[ColonnaSceltaDalGiocatore-1]][ColonnaSceltaDalGiocatore-1]=SpazioUtente;
    IndiceColonna[ColonnaSceltaDalGiocatore-1]--;
}

void TurnoComputer()
{
    srand((unsigned)time(NULL));
    do
    {
        ColonnaSceltaDalComputer=rand()%7;
    }
    while(IndiceColonna[ColonnaSceltaDalComputer]<0);
    TabellaDiGioco[IndiceColonna[ColonnaSceltaDalComputer]][ColonnaSceltaDalComputer] = SpazioAvversario;
    IndiceColonna[ColonnaSceltaDalComputer]--;
}

int ControlloVincita()
{
    int riga, colonna;
    int i,j,k;
    for(riga=0; riga<6; riga++)
    {
        if(TabellaDiGioco[riga][0]==SpazioUtente&&TabellaDiGioco[riga][1]==SpazioUtente&&TabellaDiGioco[riga][2]==SpazioUtente&&TabellaDiGioco[riga][3]==SpazioUtente ||
                TabellaDiGioco[riga][1]==SpazioUtente&&TabellaDiGioco[riga][2]==SpazioUtente&&TabellaDiGioco[riga][3]==SpazioUtente&&TabellaDiGioco[riga][4]==SpazioUtente ||
                TabellaDiGioco[riga][2]==SpazioUtente&&TabellaDiGioco[riga][3]==SpazioUtente&&TabellaDiGioco[riga][4]==SpazioUtente&&TabellaDiGioco[riga][5]==SpazioUtente ||
                TabellaDiGioco[riga][3]==SpazioUtente&&TabellaDiGioco[riga][4]==SpazioUtente&&TabellaDiGioco[riga][5]==SpazioUtente&&TabellaDiGioco[riga][6]==SpazioUtente)
        {
            vittoria =  1;
            return 1;
        }
        else if (TabellaDiGioco[riga][0]==SpazioAvversario&&TabellaDiGioco[riga][1]==SpazioAvversario&&TabellaDiGioco[riga][2]==SpazioAvversario&&TabellaDiGioco[riga][3]==SpazioAvversario ||
                 TabellaDiGioco[riga][1]==SpazioAvversario&&TabellaDiGioco[riga][2]==SpazioAvversario&&TabellaDiGioco[riga][3]==SpazioAvversario&&TabellaDiGioco[riga][4]==SpazioAvversario ||
                 TabellaDiGioco[riga][2]==SpazioAvversario&&TabellaDiGioco[riga][3]==SpazioAvversario&&TabellaDiGioco[riga][4]==SpazioAvversario&&TabellaDiGioco[riga][5]==SpazioAvversario ||
                 TabellaDiGioco[riga][3]==SpazioAvversario&&TabellaDiGioco[riga][4]==SpazioAvversario&&TabellaDiGioco[riga][5]==SpazioAvversario&&TabellaDiGioco[riga][6]==SpazioAvversario)
        {
            vittoria = 2;
            return 2;
        }
    }
    for(colonna=0; colonna<7; colonna++)
    {
        if (TabellaDiGioco[0][colonna]==SpazioUtente&&TabellaDiGioco[1][colonna]==SpazioUtente&&TabellaDiGioco[2][colonna]==SpazioUtente&&TabellaDiGioco[3][colonna]==SpazioUtente ||
            TabellaDiGioco[1][colonna]==SpazioUtente&&TabellaDiGioco[2][colonna]==SpazioUtente&&TabellaDiGioco[3][colonna]==SpazioUtente&&TabellaDiGioco[4][colonna]==SpazioUtente ||
            TabellaDiGioco[2][colonna]==SpazioUtente&&TabellaDiGioco[3][colonna]==SpazioUtente&&TabellaDiGioco[4][colonna]==SpazioUtente&&TabellaDiGioco[5][colonna]==SpazioUtente)
        {
            vittoria =  1;
            return 1;
        }
        else if (TabellaDiGioco[0][colonna]==SpazioAvversario&&TabellaDiGioco[1][colonna]==SpazioAvversario&&TabellaDiGioco[2][colonna]==SpazioAvversario&&TabellaDiGioco[3][colonna]==SpazioAvversario ||
                 TabellaDiGioco[1][colonna]==SpazioAvversario&&TabellaDiGioco[2][colonna]==SpazioAvversario&&TabellaDiGioco[3][colonna]==SpazioAvversario&&TabellaDiGioco[4][colonna]==SpazioAvversario ||
                 TabellaDiGioco[2][colonna]==SpazioAvversario&&TabellaDiGioco[3][colonna]==SpazioAvversario&&TabellaDiGioco[4][colonna]==SpazioAvversario&&TabellaDiGioco[5][colonna]==SpazioAvversario)
        {
            vittoria = 2;
            return 2;
        }
    }

    ///////////////////////////////DIAGONALI//////////////////////////
    riga=0;
    colonna=0;
    for(colonna=6; colonna>=3; colonna--)
    {
        for(riga=5; riga>=3; riga--)
        {
            if(TabellaDiGioco[riga][colonna]==SpazioUtente&&TabellaDiGioco[riga-1][colonna-1]==SpazioUtente&&TabellaDiGioco[riga-2][colonna-2]==SpazioUtente&&TabellaDiGioco[riga-3][colonna-3]==SpazioUtente)
            {
                vittoria = 1;
                return 1;
            }
            else if(TabellaDiGioco[riga][colonna]==SpazioAvversario&&TabellaDiGioco[riga-1][colonna-1]==SpazioAvversario&&TabellaDiGioco[riga-2][colonna-2]==SpazioAvversario&&TabellaDiGioco[riga-3][colonna-3]==SpazioAvversario)
            {
                vittoria = 2;
                return 2;
            }
        }
    }
    for(colonna=0; colonna<4; colonna++)
    {
        for(riga=5; riga>=3; riga--)
        {
            if(TabellaDiGioco[riga][colonna]==SpazioUtente&&TabellaDiGioco[riga-1][colonna+1]==SpazioUtente&&TabellaDiGioco[riga-2][colonna+2]==SpazioUtente&&TabellaDiGioco[riga-3][colonna+3]==SpazioUtente)
            {
                vittoria = 1;
                return 1;
            }
            else if(TabellaDiGioco[riga][colonna]==SpazioAvversario&&TabellaDiGioco[riga-1][colonna+1]==SpazioAvversario&&TabellaDiGioco[riga-2][colonna+2]==SpazioAvversario&&TabellaDiGioco[riga-3][colonna+3]==SpazioAvversario)
            {
                vittoria = 2;
                return 2;
            }
        }
    }

}

void SetColor(short color)
{
    HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hCon,color);
}
